# Parsers from combinational netlist formats to graphs
#### Authors: Th�o Basty, Brice Colombier, Lilian Bossuet
#### Laboratoire Hubert Curien
#### 18 rue Pr. Beno�t Lauras, 42000 Saint-Etienne, FRANCE

These Python scripts can convert combinational netlist files to directed acyclic graphs in the [igraph](http://igraph.org/python/) format.

So far it handles the following netlist formats:
- **bench**: ISCAS'85 and ITC'99 netlists format
- **BLIF**: Berkeley Logic Interchange Format
- **SLIF**: Stanford Logic Interchange Format
- **EDIF**: Electronic Design Interchange Format
- **VHDL** (dataflow and structural)
- **Verilog** (dataflow and structural)
- **Xilinx EDIF**: Netlist mapped to Xilinx LUTs
